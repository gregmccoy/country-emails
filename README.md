# Country Emails

This is basically just a big find and replace script. You can changes the
replace list through the web interface. You also get a nice preview and edit 
to make changes on the fly.

# Install

## Install enchant

Enchant is used for spell checking.

Centos
`yum install enchant`

Fedora
`dnf install enchant`

## Init db
`python initdb.py`

## Run
`FLASK_APP=views.py flask run` 


